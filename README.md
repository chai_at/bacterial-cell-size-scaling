# Bacterial cell size scaling

The code runs all the calculations used in the figures to the manuscript by the authors Tanvi Kale, Dhruv Khatri and Chaitanya A. Athale titled "E. coli surface area scaling with volume modeled by cell shape variability and regulation".